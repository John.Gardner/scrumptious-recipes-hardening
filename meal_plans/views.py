# from django.shortcuts import render
# from django.shortcuts import redirect
# from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView

from meal_plans.models import MealPlan


# Create your views here.
class MealPlanListView(ListView):
    model = MealPlan
    template_name = "meal_plans/list.html"


class MealPlanCreateView(CreateView):
    pass


class MealPlanDetailView(DetailView):
    pass


class MealPlanEditView(UpdateView):
    pass


class MealPlanDeleteView(DeleteView):
    pass
