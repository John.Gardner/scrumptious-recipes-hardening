from django.urls import path

urlpatterns = [
    path("meal_plans/", MealPlansListView.as_view(), name="meal_plans_list"),
    path(
        "meal_plans/create/", MealPlanCreateView.as_view(), name="meal_plan_new"
    ),
    path(
        "meal_plans/<int:pk>/",
        MealPlanDetailView.as_view(),
        name="meal_plan_detail",
    ),
    path(
        "meal_plans/<int:pk>/edit/",
        MealPlanEditView.as_view(),
        name="meal_plan_edit",
    ),
    path(
        "meal_plans/<int:pk>/delete/",
        MealPlanDeleteView.as_view(),
        name="meal_plan_delete",
    ),
]
